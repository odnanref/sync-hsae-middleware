
package main.scala.far.org.middleware

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap

import java.sql.{Connection, DriverManager, ResultSet, PreparedStatement, Statement};

case class Paciente(
    sus:String
    ,nome:String
    ,dataNascimento: String
    ,sexo: String
    ,tiposanguineo:String
    ,Cor:String
    )


class PacienteModel(conn:Connection) {

	def getDetalhesPaciente(idPaciente: Int) :Paciente = {
	  val sqll = """
	SELECT [idPaciente]
      ,[Paciente]
      ,[DataNascimento]
      ,[idEstadoCivil]
      ,[Identidade]
      ,[IdentidadeOrgaoEmissoruf]
      ,[Cpf]
      ,[CartaoNacionalSus]
      ,[idTipoLogradouro]
      ,[Endereco]
      ,[Numero]
      ,[Complemento]
      ,[Bairro]
      ,[Cidade]
      ,[Estado]
      ,[Cep]
      ,[TelefoneResidencial]
      ,[TelefoneCelular]
      ,[TelefoneComercial]
      ,[FiliacaoPai]
      ,[FiliacaoMae]
      ,[BloqueioPaciente]
      ,[BloqueioPacienteMotivo]
      ,[idTipoDocumento]
      ,[PacienteRN]
      ,[CodigoAntigo]
      ,[CodigoAntigo2]
      ,[NovoBairro]
      ,[Observacao]
      ,[Alergia]
      ,[Nacionalidade]
      ,[Profissao]
      ,[Filhos]
      ,[idGrauinstrucao]
      ,[TipoSanguineo]
      ,[HistoricoAntigo]
      ,[Email]
      ,[idPacienteCondicao]
      ,[SMGH].[dbo].[Cad_ANSSexualidade].[Codigo] as sexualidade
	  ,[Cor]
  FROM [SMGH].[dbo].[Cad_Paciente]
  INNER JOIN [SMGH].[dbo].[Cad_ANSSexualidade]
  ON ([SMGH].[dbo].[Cad_ANSSexualidade].idSexualidade = [SMGH].[dbo].[Cad_Paciente].idSexualidade)
  INNER JOIN [SMGH].[dbo].[Cad_ANSTipoSanguineo]
  ON ([SMGH].[dbo].[Cad_ANSTipoSanguineo].idTipoSanguineo = [SMGH].[dbo].[Cad_Paciente].idTipoSanguineo)
  INNER JOIN [SMGH].[dbo].[Cad_ANSCor]
  ON ([SMGH].[dbo].[Cad_ANSCor].idCor = [SMGH].[dbo].[Cad_Paciente].idCor)
 	    WHERE idPaciente = ?
	    """;

	  try {
	    val statement = conn.prepareStatement(sqll)
	    // execute query
	    statement.setInt(1, idPaciente);
	    val rs = statement.executeQuery()
	    rs.next;
	    return new Paciente(
	        rs.getString("CartaoNacionalSus")
		    ,rs.getString("Paciente")
		    ,rs.getString("DataNascimento")
		    ,rs.getString("sexualidade")
		    ,rs.getString("TipoSanguineo")
		    ,rs.getString("Cor")
		   );

	  }
	}
}
