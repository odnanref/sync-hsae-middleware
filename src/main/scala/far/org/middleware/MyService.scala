package main.scala.far.org.middleware

import akka.actor.Actor
import spray.routing._
import spray.http._
import MediaTypes._
import HttpCharsets._

import spray.json.DefaultJsonProtocol
import spray.httpx.unmarshalling._
import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.util._

object MyJsonProtocol extends DefaultJsonProtocol {
  implicit val BAMFormat = jsonFormat3(Bam)
}

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor extends Actor with MyService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}


// this trait defines our service behavior independently from the service actor
trait MyService extends HttpService {

  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) { // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <h1>Say hello to <i>spray-routing</i> on <i>spray-can</i>!</h1>
              </body>
            </html>
          }
        }
      }
    } ~
  path("bam" / IntNumber) { bam =>
    get {
      import MyJsonProtocol._
      val Atendimento = MainBam.getBamByNumber(bam.toString);

      respondWithMediaType(`application/json`) {
      complete(
    	"setBam({ \"idPaciente\":" + Atendimento.idPaciente + ","
          + "\"DataAtendimento\": \""+Atendimento.DataAtendimento + "\","
          + "\"doctorname\": \""+Atendimento.PrestadorMedico + "\","
          + "\"doctorcrm\": \""+Atendimento.PrestadorMedicoCRM + "\","
          + "\"NumeroGuia\": " + bam + ","
          + "\"NumeroProntuario\": " + Atendimento.NumeroGuia + ","
          + "\"paciente\": { "
          + "\"idPaciente\":" + Atendimento.idPaciente + ","
          + "\"sus\":" + Atendimento.Paciente
              .map{_.sus}.getOrElse("ERRO A OBTER cartao sus")  + ","
          + "\"name\": \"" + Atendimento.Paciente
              .map{_.nome}.getOrElse("ERRO A OBTER NOME") + "\","
          + "\"birthdate\": \""+Atendimento.Paciente
              .map{_.dataNascimento}.getOrElse("ERRO A OBTER data nascimento") + "\","
          + "\"color\": \"" + Atendimento.Paciente
              .map{_.Cor}.getOrElse("ERRO A OBTER Cor da pele") + "\","
          + "\"sex\": \"" + Atendimento.Paciente
              .map{_.sexo}.getOrElse("ERRO A OBTER sexo do paciente") + "\","
          + "\"bloodtype\": \"" + Atendimento.Paciente
              .map{_.tiposanguineo}.getOrElse("ERRO A OBTER tipo sanguineo") + "\""
          + "}"
          + "});"
    	);
      }
    }
  }
}
