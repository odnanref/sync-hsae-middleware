
package main.scala.far.org.middleware

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import java.sql.{Connection, DriverManager, ResultSet, PreparedStatement, Statement}
import main.scala.far.org.middleware.Paciente
import main.scala.far.org.middleware.PacienteModel

case class Bam(
    numerobam:String
    ,NumeroGuia:String
    ,idP:Int
) {
  var idPaciente:Int = idP;
  var Paciente: Option[Paciente] = None;
  var DataAtendimento:String = "";
  var PrestadorMedico:String = "";
  var PrestadorMedicoCRM:String = "";
}

class BamModel (conn:Connection) {

  def getPacientIdByAtendimento(numerobam:String) :Bam = {
	  val SQLAtendimento = """
		SELECT idAtendimento ,NumeroProntuario ,NumeroGuia ,idPaciente
	      ,idUnidadeAtendimento ,DataAtendimento ,HoraAtendimento ,HoraAtendimentoNormal
	      ,HoraAtendimentoUrgencia ,idCaraterSolicitacao ,DataEmissaoGuia
	      ,idPacientePlano ,MedicoSolicitante ,MedicoSolicitanteRegistro
	      ,idMedicoSolicitante ,idEspecialidadeSolicitante ,idCbosSolicitante
	      ,idMedicoExecutante ,idEspecialidadeExecutante ,SenhaDataAutorizacao
	      ,SenhaAutorizacao ,SenhaDataValidade ,Gravado ,ContratadoOperadora
	      ,ContratadoSolicitante ,ContratadoCNES ,idMedicoSolicitanteConselho
	      ,idMotivoDesconto ,NumeroDocumento ,DataFaturamento ,idUsuarioFaturamento
	      ,DataCompetenciaFaturamento ,NumeroLoteTuss ,DataLoteTuss
	      ,DataLoteConfirmacaoTuss ,idPlanoConvenio ,idTipoDoenca
	      ,idTipoAtendimento ,idTipoConsulta ,idCid10Principal
	      ,idCid10Dois ,idCid10Tres ,idCid10Quatro ,QtdeUnidadeTempoDoenca
	      ,idUnidadeTempoDoenca ,DataInclusao ,idUsuarioInclusao
	      ,idMedicoRealizado ,SenhaNomeResponsavelAutorizacao
	      ,ObservacaoGuia ,idEmpresaFilial ,NumeroGuiaPrincipal
	      ,Observacao ,AtendidoComReceituario ,DataCompetenciaMedico
	      ,idTipoOrigem ,idTipoAcomodacao ,idTipoAcomodacaoLocalizacao
	      ,idTipoInternacao ,idRegimeInternacao ,idTipoFaturamento
	      ,QtdeDiariaSolicitada ,QtdeDiariaAutorizada ,idTipoAcomodacaoAutorizada
	      ,NomeResponsavelAcompanhante ,idTipoDocumentoResponsavelAcompanhante
	      ,NumeroDocumentoResponsavelAcompanhante ,TelefoneResponsavelAcompanhante
	      ,DataSaida ,DataSaidaInternacao ,idMedicoResponsavelSaidaInternacao
	      ,DataOcorrenciaObito ,idMedicoResponsavelAtestadoObito ,idUsuarioLiberouAlta
	      ,idUsuarioLiberouPaciente ,NunGuiaSolicitacaoInternacao
	      ,DataNascimentoRN ,idSexoRN ,PesoRN ,EstaturaRN ,PCRN ,PTRN
	      ,idPrestadorServicoHospitalar
	      ,NomeResponsavelPrestadorHospitalar
	      ,IdentCrmResponsavelPrestadorHospitalar
	      ,DataInclusaoSaida
	      ,AtoConfirmaSaida
	      ,AtoConfirmaLiberacao
	      ,idAtendimentoSeriado
	      ,Internacao
	      ,idMedicoExecutanteOriginal
	      ,PacienteLeitoLiberado
	      ,PacienteSaidaLiberado
	      ,RegistroOcorrenciaSaida
	      ,idAtendimentoServicoSubstituido
	      ,DataEmissaoLaudo
	      ,idAgenda
	      ,NumeroFilmeLaudo
	      ,DataConferencia
	      ,idUsuarioConferencia
	      ,DataImpressaoGuia
	      ,AtendidoCooperado
	      ,NumeroProtocolo
	    ,prestmed.PrestadorMedico as PrestadorMedico 
	    ,prestmed.CRM as CRM
	  FROM [SMGH].[dbo].[Tab_Atendimento]
	    INNER JOIN [SMGH].[dbo].[Cad_PrestadorMedico] as prestmed
			  ON prestmed.idPrestadorMedico = idMedicoSolicitante
	  WHERE NumeroProntuario = ?
	""";
	  // Get the bam details
	  try {
	    val statement = conn.prepareStatement(SQLAtendimento)
	    // execute query
	    statement.setInt(1, numerobam.toInt);
	    val rs = statement.executeQuery()
	    rs.next;
	    val bam = new Bam(rs.getString("NumeroProntuario")
	    		  ,rs.getString("NumeroGuiaPrincipal")
	    		  ,rs.getInt("idPaciente")
	      );
	    bam.PrestadorMedico = rs.getString("PrestadorMedico");
	    bam.PrestadorMedicoCRM = rs.getString("CRM");
	    bam.DataAtendimento = rs.getString("DataAtendimento");
	    if (bam.idPaciente > 0) {
	      val pmodel = new PacienteModel(conn);
	      bam.Paciente = Some(pmodel.getDetalhesPaciente(bam.idPaciente));
	      //println(" >> " + pmodel.getDetalhesPaciente(bam.idPaciente).nome );
	  	}
	    return bam;
	  }
  }

}
